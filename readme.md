# Vite插件简介
将Vite开发的Vue项目编译类型声明文件*.d.ts

## 如何使用

### 安装
```shell
# 编译阶段使用，所以只需要按照到开发依赖中
yarn add vite-plugin-build-types -D
```
### 在vite.config.ts中引用
```ts
// vite.config.ts
// 无关代码省略，仅保留关键代码

import VitePluginBuildTypes from "vite-plugin-build-types"

export default defineConfig({
  build: {
    // 如果下方插件配置的aim目录与vite编译输出目录一致，则必须配置该项为false，目录不一致，则不需该配置
    emptyOutDir: false,
    rollupOptions: {
      plugins: [
        VitePluginBuildTypes({
          // 项目根目录
          root: path.resolve(__dirname),
          // 源代码目录 string | string[]
          source: path.resolve(__dirname, "src"),
          // 输出目录
          aim: path.resolve(__dirname, "dist")
        })
      ]
    }
    // build end
  }
})
```

## 结果展示

![结果展示1](https://gitee.com/bitem/vite-plugin-build-types/raw/master/images/result-console.png)

![结果展示2](https://gitee.com/bitem/vite-plugin-build-types/raw/master/images/result-file.png)


## 源码地址
[https://gitee.com/bitem/vite-plugin-build-types](https://gitee.com/bitem/vite-plugin-build-types)
